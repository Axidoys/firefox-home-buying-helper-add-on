
document.body.style.border = "5px solid orange";

pageContent = document.body.innerHTML
pageText = document.body.innerText.normalize("NFD").replace(/[\u0300-\u036f]/g, "")

//surface
const matchSurface = pageText.match(/([0-9]+)\sm²/);
surface = ""
if(matchSurface != null){
    surface = matchSurface[1]
}

//surface field
const matchSurfaceField = pageText.match(/terrain\s([0-9]+)\sm²/i);
surfaceField = ""
if(matchSurfaceField != null){
    surfaceField = matchSurfaceField[1]
}

//rooms
const matchRooms = pageContent.match(/([0-9]+)\s?pièces/i);
rooms = ""
if(matchRooms != null){
    rooms = parseInt(matchRooms[0])
}

//price
const matchPrice = pageText.match(/([0-9]{3})\s?[0-9]{3}\s?€/); // todo à tester, ajout option 'g', autre // g remove capturing...
price = ""
if(matchPrice != null){
    console.log("prix", matchPrice)
    price = `${matchPrice[1]}`
    //price = `${Math.max(...matchPrice.map(el => el.match(/([0-9]{3})/)[0]))}k€` //TODO remove because other ads and take first ?
}

//classes
energyClass = ""
//others
//leboncoin
const matchEnergyClassLeboncoin = pageContent.match(/styles_letter([A-Z]).{1,10}styles_active/);
if(matchEnergyClassLeboncoin != null){
    energyClass = `${matchEnergyClassLeboncoin[1]}`
}

//url
url = document.URL

//city
city = ""
const matchCity1 = pageText.match(/([A-Z]{4,20})\s?\(?[0-9]{2}\)?/i);
const matchCity2 = pageText.match(/([A-Z]{4,20})\s?\(?[0-9]{5}\)?/i);
if(matchCity1 != null && (matchCity2 == null || matchCity1.index > matchCity2.index)){
    console.log(matchCity1)
    city = matchCity1[1]
    //city = `${matchCity.map(el => el.match(/([A-Z]+)/)[0])}`
}
else if(matchCity2 != null){
    console.log(matchCity2)
    city = matchCity2[1]
}

//heating
heating = ""
heatings = []
candidatesHeating = ["Elec", "Gaz", "Bois", "Cheminee", "Chaudiere"] //todo gaz, avoid "Gaz à effet de serre"
// todo maybe go to "chauffage [a-z]*"
candidatesHeating.forEach(candidate => {
    const matchOneHeating = pageContent.match(RegExp(candidate, "i"));
    if(matchOneHeating != null){
        heatings.push(candidate)
    }
})
heating = heatings.join(", ")

//todo Ville (CP)     -> to be checked
//todo chauffage     -> to be checked
//todo classe énergétique -> to be checked
//todo référence ???
//todo url              -> to be checked
//todo lorsque multiple surface/prix/ ...

// header : Note, Surface int, Surface tot, Pièce, Lieu, Classe énergétique, Chauffage, Prix, Référence URL
newClipboard = ["", `${surface}`, `${surfaceField}`, `${rooms}`, `${city}`, `${energyClass}`, `${heating}`, `${price}`, `${url}`]

newClipboardString = newClipboard
    .map(e => e.normalize("NFD")
        .replace(/[\u0300-\u036f]/g, "")
        .replace("\n", "")
        .replace("\t", ""))
    .join('\t')

navigator.clipboard.writeText(newClipboardString).then(() => {
    document.body.style.border = "5px solid green";
}, () => {
    document.body.style.border = "5px solid red";
});
