browser.contextMenus.create({
    id: "house-copy",
    title: "Copy house information"
});

browser.contextMenus.onClicked.addListener((info, tab) => {
    if (info.menuItemId === "house-copy") {
        browser.tabs.executeScript({
            file: "home-buying-helper.js"
        });
    }
});